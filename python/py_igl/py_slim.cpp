// This file is part of libigl, a simple c++ geometry processing library.
//
// Copyright (C) 2017 Sebastian Koch <s.koch@tu-berlin.de> and Daniele Panozzo <daniele.panozzo@gmail.com>
//
// This Source Code Form is subject to the terms of the Mozilla Public License
// v. 2.0. If a copy of the MPL was not distributed with this file, You can
// obtain one at http://mozilla.org/MPL/2.0/.
py::class_<igl::SLIMData> SLIMData(m, "SLIMData");

SLIMData
.def(py::init<>())
.def_readwrite("V", &igl::SLIMData::V)
.def_readwrite("F", &igl::SLIMData::F)
.def_readwrite("slim_energy", &igl::SLIMData::slim_energy)

.def_property("b",
              [](const igl::SLIMData &data) { return Eigen::MatrixXi(data.b); },
              [](igl::SLIMData &data, const Eigen::MatrixXi &b) {
                assert_is_VectorX("b", b);
                data.b = Eigen::VectorXi(b.cast<int>());
              })
.def_readwrite("bc", &igl::SLIMData::bc)
.def_readwrite("soft_const_p", &igl::SLIMData::soft_const_p)

.def_readwrite("exp_factor", &igl::SLIMData::exp_factor)
.def_readwrite("mesh_improvement_3d", &igl::SLIMData::mesh_improvement_3d)

.def_readwrite("V_o", &igl::SLIMData::V_o)
.def_readwrite("energy", &igl::SLIMData::energy)
.def_readwrite("energy_per_face", &igl::SLIMData::energy_per_face)
.def_readwrite("area_per_face", &igl::SLIMData::energy_per_face)
.def_readwrite("sigma", &igl::SLIMData::sigma)

.def_readwrite("M", &igl::SLIMData::M)
.def_readwrite("mesh_area", &igl::SLIMData::mesh_area)
.def_readwrite("avg_edge_length", &igl::SLIMData::avg_edge_length)
.def_readwrite("v_num", &igl::SLIMData::v_num)
.def_readwrite("f_num", &igl::SLIMData::f_num)
.def_readwrite("proximal_p", &igl::SLIMData::proximal_p)

.def_readwrite("WGL_M", &igl::SLIMData::WGL_M)
.def_readwrite("rhs", &igl::SLIMData::rhs)
.def_readwrite("Ri", &igl::SLIMData::Ri)
.def_readwrite("Ji", &igl::SLIMData::Ji)
.def_readwrite("W", &igl::SLIMData::W)

.def_readwrite("Dx", &igl::SLIMData::Dx)
.def_readwrite("Dy", &igl::SLIMData::Dy)
.def_readwrite("Dz", &igl::SLIMData::Dz)

.def_readwrite("f_n", &igl::SLIMData::f_n)
.def_readwrite("v_n", &igl::SLIMData::v_n)

.def_readwrite("first_solve", &igl::SLIMData::first_solve)
.def_readwrite("has_pre_calc", &igl::SLIMData::has_pre_calc)
.def_readwrite("dim", &igl::SLIMData::dim);



m.def("slim_calc_energy", []
(
  const Eigen::MatrixXd& V,
  const Eigen::MatrixXi& F,
  const Eigen::MatrixXd& V_init,
  igl::SLIMData& data,
  igl::MappingEnergyType slim_energy,
  double soft_p
)
{
  // assert_is_VectorX("b",b);
  // Eigen::VectorXi bv;
  // if (b.size() != 0)
  //   bv = b;
  igl::slim_calc_energy(V, F, V_init, data, slim_energy, soft_p);
}, __doc_igl_slim,
py::arg("V"), py::arg("F"), py::arg("V_init"), py::arg("data"), py::arg("slim_energy"), py::arg("soft_p"));


m.def("slim_precompute", []
(
  const Eigen::MatrixXd& V,
  const Eigen::MatrixXi& F,
  const Eigen::MatrixXd& V_init,
  igl::SLIMData& data,
  igl::MappingEnergyType slim_energy,
  double soft_p
)
{
  // assert_is_VectorX("b",b);
  // Eigen::VectorXi bv;
  // if (b.size() != 0)
  //   bv = b;
  igl::slim_precompute(V, F, V_init, data, slim_energy, soft_p);
}, __doc_igl_slim,
py::arg("V"), py::arg("F"), py::arg("V_init"), py::arg("data"), py::arg("slim_energy"), py::arg("soft_p"));


m.def("slim_solve", []
(
  igl::SLIMData& data,
  int iter_num
)
{
  return igl::slim_solve(data, iter_num);
}, __doc_igl_slim,
py::arg("data"), py::arg("iter_num"));



m.def("slim_update_weights_and_closest_rotations_with_jacobians", []
(
  const Eigen::MatrixXd& Ji,
  igl::MappingEnergyType slim_energy,
  double exp_factor,
  Eigen::MatrixXd& W,
  Eigen::MatrixXd& Ri
)
{
  igl::slim_update_weights_and_closest_rotations_with_jacobians(Ji, slim_energy, exp_factor, W, Ri);
}, __doc_igl_slim,
py::arg("Ji"), py::arg("slim_energy"), py::arg("exp_factor"), py::arg("W"),py::arg("Ri"));


m.def("slim_buildA", []
(
  const Eigen::SparseMatrix<double>& Dx,
  const Eigen::SparseMatrix<double>& Dy,
  const Eigen::SparseMatrix<double>& Dz,
  const Eigen::MatrixXd& W,
  std::vector<Eigen::Triplet<double> > & IJV
)
{
  igl::slim_buildA(Dx, Dy, Dz, W, IJV);
}, __doc_igl_slim,
py::arg("Dx"), py::arg("Dy"), py::arg("Dz"), py::arg("W"), py::arg("IJV"));

m.def("slim_lscm", []
(
  const Eigen::MatrixXd& V,
  const Eigen::MatrixXi& F,
  const Eigen::MatrixXi& b,
  const Eigen::MatrixXd& bc,
  Eigen::MatrixXd& V_uv
)
{
  assert_is_VectorX("b",b);
  return igl::lscm(V,F,b,bc,V_uv);
}, __doc_igl_lscm,
py::arg("V"), py::arg("F"), py::arg("b"), py::arg("bc"), py::arg("V_uv"));
