// This file is part of libigl, a simple c++ geometry processing library.
//
// Copyright (C) 2017 Sebastian Koch <s.koch@tu-berlin.de> and Daniele Panozzo <daniele.panozzo@gmail.com>
//
// This Source Code Form is subject to the terms of the Mozilla Public License
// v. 2.0. If a copy of the MPL was not distributed with this file, You can
// obtain one at http://mozilla.org/MPL/2.0/.
py::enum_<igl::MappingEnergyType>(m, "MappingEnergyType")
    .value("ARAP", igl::MappingEnergyType::ARAP)
    .value("LOG_ARAP", igl::MappingEnergyType::LOG_ARAP)
    .value("SYMMETRIC_DIRICHLET", igl::MappingEnergyType::SYMMETRIC_DIRICHLET)
    .value("CONFORMAL", igl::MappingEnergyType::CONFORMAL)
    .value("EXP_CONFORMAL", igl::MappingEnergyType::EXP_CONFORMAL)
    .export_values();
